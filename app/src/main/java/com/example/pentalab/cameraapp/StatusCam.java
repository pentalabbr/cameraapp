package com.example.pentalab.cameraapp;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Guillermo on 31-Aug-16.
 */
public class StatusCam extends AsyncTask<String, Void, Boolean> {

    Boolean ok = false;

    @Override
    protected Boolean doInBackground(String... params) {

        try {
            ok = false;
            //String url = "http://192.168.0.12/setSystemMotion";
            HttpURLConnection con = (HttpURLConnection) (new URL(params[0])).openConnection();

            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setConnectTimeout(1000);

            if (params.length < 1) {
                return false;
            }

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            String urlParameters = "";
            if (params.length > 1)
                urlParameters = params[1];

            wr.writeBytes(urlParameters);
            wr.close();
//            else{
//                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//                String urlParameters ="";
//                wr.writeBytes(urlParameters);
//                wr.close();
//            }
            String text = "";
//            int responseCode = con.getResponseCode();
//            System.out.println("\nSending 'POST' request to URL : " + params[0]);
//            System.out.println("Post parameters : " + urlParameters);
//            System.out.println("Response Code : " + responseCode);
//            System.out.println("Response mess : " +  con.getResponseMessage());

            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            //StringBuilder sb = new StringBuilder();
            String line = null;

            //Read Server Response
            while ((line = reader.readLine()) != null) {
                // Append server response in string
                //sb.append(line + "\n");
                if (line.contains("input type=radio name=MotionDetectionEnable value=1 checked")) {
                    ok = true;
                }
            }


            //System.out.println(sb.toString());
        } catch (ProtocolException e1) {
            e1.printStackTrace();
            return false;
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
            return false;
        } catch (IOException e1) {
            e1.printStackTrace();
            return false;
        } catch (Exception e) {
            return false;
        }

        return ok;
    }
}
