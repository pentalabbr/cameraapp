package com.example.pentalab.cameraapp.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.example.pentalab.cameraapp.ConnectionTask;
import com.example.pentalab.cameraapp.ConnectionTest;
import com.example.pentalab.cameraapp.DbActions.CameraActions;
import com.example.pentalab.cameraapp.Model.Cameras;
import com.example.pentalab.cameraapp.R;
import com.example.pentalab.cameraapp.StatusCam;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;


public class GerenciarCamerasActivity extends Activity {

    List<Cameras> lista = new ArrayList<>();
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        CameraActions cameraActions = new CameraActions(this);

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("admin", "123456".toCharArray());
            }
        });


        lista = cameraActions.selectCameras();
        switch (lista.size()){
            case 1:
                ((Button)findViewById(R.id.attiva2)).setEnabled(false);
                ((Button)findViewById(R.id.Attiva3)).setEnabled(false);
                ((Button)findViewById(R.id.Attiva4)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton2)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton3)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton4)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva2)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva3)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva4)).setEnabled(false);
                break;
            case 2:
                ((Button)findViewById(R.id.Attiva3)).setEnabled(false);
                ((Button)findViewById(R.id.Attiva4)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton3)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton4)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva3)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva4)).setEnabled(false);
                break;
            case 3:
                ((Button)findViewById(R.id.Attiva4)).setEnabled(false);
                ((ImageView)findViewById(R.id.imageButton4)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva2)).setEnabled(false);
                ((Button)findViewById(R.id.Disattiva4)).setEnabled(false);
                break;
        }
        bt = ((Button) findViewById(R.id.Status1));
        bt.setOnClickListener(status);
    }

    View.OnClickListener status = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Status1(v);
        }
    };

    public void Cam1On(View v) {
        try {

            if (lista.size() >= 1) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];

                params[0] = lista.get(0).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=1&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get().contains("ok")) {
                    ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_green);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }

    public void Cam1Off(View v) {
        try {

            if (lista.size() >= 1) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(0).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=0&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get().contains("ok")) {
                    ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_red);

                }
            } else

            {
                ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_gray);
            }

            bt.callOnClick();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }


    public void Status1(View v) {
        try {
            int i = 0;

            String[] params = new String[1];
            for (Cameras cam : lista) {
                try {
                    StatusCam task = new StatusCam();
//                    param[0] = lista.get(i).getIP() + "motion.htm";
                    //  if ((test.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, param).get())) {
                    params[0] = cam.getIP() + "motion.htm";
                    //params[1] = "";// "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm";
                    Boolean ok = task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get();

                    System.out.println(ok);
                    if (ok) {
                        switch (i) {
                            case 0:
                                ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_green);
                                break;
                            case 1:
                                ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_green);
                                break;
                            case 2:
                                ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_green);
                                break;
                            case 3:
                                ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_green);
                                break;
                        }
                    } else {

                        switch (i) {
                            case 0:
                                ((ImageView) (findViewById(R.id.imageButton1))).setImageResource(R.mipmap.led_red);
                                break;
                            case 1:
                                ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_red);
                                break;
                            case 2:
                                ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_red);

                                break;
                            case 3:
                                ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_red);
                                break;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                i++;
            }

        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
        catch (Exception e) {

        }

    }


    public void Cam2ON(View v) {

        try {

            if (lista.size() >= 2) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(1).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=1&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=90&ConfigSystemMotion=Save";
                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_green);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }

    public void Cam2OFF(View v) {

        try {

            if (lista.size() >= 2) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(1).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=0&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_red);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton2))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }


    public void Cam3On(View v) {

        try {

            if (lista.size() >= 3) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(2).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=1&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.execute(params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_green);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }

    public void Cam3OFF(View v) {

        try {

            if (lista.size() >= 3) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(2).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=0&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";
                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_red);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton3))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }

    public void Cam4ON(View v) {

        try {

            if (lista.size() >= 4) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(3).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=1&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_green);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }

    public void Cam4OFF(View v) {

        try {

            if (lista.size() >= 4) {
                ConnectionTask task = new ConnectionTask();
                String[] params = new String[2];
                params[0] = lista.get(3).getIP() + "setSystemMotion";
                params[1] = "ReplySuccessPage=motion.htm&ReplyErrorPage=motion.htm&MotionDetectionEnable=0&MotionDetectionScheduleDay=0&MotionDetectionScheduleMode=0&MotionDetectionSensitivity=65&ConfigSystemMotion=Save";

                if (task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params).get() == "ok") {
                    ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_red);
                }
            } else {
                ((ImageView) (findViewById(R.id.imageButton4))).setImageResource(R.mipmap.led_gray);
            }
            bt.callOnClick();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
    }

    public void Menu(View v) {

        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bt.callOnClick();
    }
}
