package com.example.pentalab.cameraapp.Activity;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Button;

import com.example.pentalab.cameraapp.DbActions.AppConfigActions;
import com.example.pentalab.cameraapp.Model.AppConfig;
import com.example.pentalab.cameraapp.R;

public class ConfiguracoesActivity extends AppCompatActivity {

    EditText login;
    EditText senha;
    Button atualizarLogin;
    AppConfigActions appConfigActions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);

        login = (EditText) findViewById(R.id.upLogin);
        senha = (EditText) findViewById(R.id.upPass);
        atualizarLogin = (Button) findViewById(R.id.atualizarLogin);
        atualizarLogin.setOnClickListener(atualizaLoginV);
        appConfigActions = new AppConfigActions(this);
    }

    OnClickListener atualizaLoginV = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (login.getText().toString() != "" && senha.getText().toString() != "" && !login.getText().toString().isEmpty() && !senha.getText().toString().isEmpty()) {
                AppConfig app = new AppConfig();
                app.setUsuario(login.getText().toString());
                app.setSenha(senha.getText().toString());
                appConfigActions.updateAppConfig(app);
            }else{
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(ConfiguracoesActivity.this).
                                setTitle("Erro").
                                setMessage("Usuário e senha não podem receber valores vazios!").
                                setPositiveButton("OK", null);
                builder.create().show();
            }
        }
    };

}
