package com.example.pentalab.cameraapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pentalab.cameraapp.DbActions.AppConfigActions;
import com.example.pentalab.cameraapp.DbActions.CameraActions;
import com.example.pentalab.cameraapp.Model.AppConfig;
import com.example.pentalab.cameraapp.DbActions.DbHelper;
import com.example.pentalab.cameraapp.Model.Cameras;
import com.example.pentalab.cameraapp.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void login(View view){
        EditText user = (EditText) findViewById(R.id.userEditText);
        CameraActions cameraActions = new CameraActions(this);
        AppConfigActions appConfigActions = new AppConfigActions(this);


        if(appConfigActions.selectAppConfig().size()==0)
            appConfigActions.insertAppConfig(new AppConfig("admin","123456"));
        if(cameraActions.selectCameras().size()==0) {
            cameraActions.insertCamera(new Cameras("http://192.168.1.233/", "Sala"));
            cameraActions.insertCamera(new Cameras("http://192.168.1.233/", "Quarto"));
            cameraActions.insertCamera(new Cameras("http://192.168.1.233/", "Varanda"));
            cameraActions.insertCamera(new Cameras("http://192.168.1.233/", "Rua"));
        }

        EditText password = (EditText) findViewById(R.id.passwordEditText);
        List<AppConfig> appConfigList = appConfigActions.selectAppConfig();

        if(user.getText().toString().equals(appConfigList.get(0).getUsuario())
                && password.getText().toString().equals(appConfigList.get(0).getSenha())){
            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            startActivity(intent);
        }
        else {
            TextView textPassword = (TextView) findViewById(R.id.textView3);
            textPassword.setVisibility(View.VISIBLE);
        }


    }

}
