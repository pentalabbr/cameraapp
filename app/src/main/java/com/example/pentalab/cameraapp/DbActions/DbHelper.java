package com.example.pentalab.cameraapp.DbActions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pentalab.cameraapp.Model.AppConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Femin on 29/08/2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String NOME_BASE = "CameraApp";
    private static final int VERSAO_DA_BASE = 5;


    public DbHelper(Context context) {
        super(context, NOME_BASE, null, VERSAO_DA_BASE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String qryCreateTabela = "CREATE TABLE if not exists Cameras("
                + "Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "Nome TEXT,"
                + "IP TEXT"
                +");";
        db.execSQL(qryCreateTabela);

        qryCreateTabela = "CREATE TABLE if not exists AppConfig("
                + "Id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "Usuario TEXT,"
                + "Senha TEXT"
                +");";
        db.execSQL(qryCreateTabela);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String qryDropTabela = "DROP TABLE if exists Cameras; DROP TABLE if exists AppConfig;";
        db.execSQL(qryDropTabela);
        onCreate(db);

    }
}
