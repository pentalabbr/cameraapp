package com.example.pentalab.cameraapp.Activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.os.SystemClock;
import android.util.Patterns;
import android.view.View;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.pentalab.cameraapp.ConnectionTest;
import com.example.pentalab.cameraapp.DbActions.CameraActions;
import com.example.pentalab.cameraapp.Model.AppConfig;
import com.example.pentalab.cameraapp.DbActions.DbHelper;
import com.example.pentalab.cameraapp.Model.Cameras;
import com.example.pentalab.cameraapp.R;
import com.example.pentalab.cameraapp.StatusCam;

public class ImageActivity extends Activity {

    //private EditText edtUrl;
    public View view;
    private Thread thr1;
    private int idCamera = 0;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Authenticator.setDefault (new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("admin", "123456".toCharArray());
            }
        });
        setContentView(R.layout.activity_image);
        context = this;
        CameraActions cameraActions = new CameraActions(this);
        List<Cameras> camerasList = cameraActions.selectCameras();

        Button button1 = (Button) findViewById(R.id.camera1);
        button1.setText(camerasList.get(0).getNome());
        Button button2 = (Button) findViewById(R.id.camera2);
        button2.setText(camerasList.get(1).getNome());
        Button button3 = (Button) findViewById(R.id.camera3);
        button3.setText(camerasList.get(2).getNome());
        Button button4 = (Button) findViewById(R.id.camera4);
        button4.setText(camerasList.get(3).getNome());


        button1.setOnClickListener(status);
        button2.setOnClickListener(status);
        button3.setOnClickListener(status);
        button4.setOnClickListener(status);

        String url = cameraActions.selectCameras().get(idCamera).getIP();

        ConnectionTest task = new ConnectionTest();
        String[] params = new String[1];
        params[0] = url;
        try {
            if(task.execute(params).get()) {
                new DownloadImagemAsyncTask().execute(
                        url + "image.jpg"
                );
            }
            else{
                System.out.println("erro");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    View.OnClickListener status = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(thr1 != null && thr1.isAlive()){
                System.out.println("pausing");
              //  thr1.interrupt();
            }

            view = v;

            switch (v.getId()) {
                case (R.id.camera1):
                    idCamera = 0;
                    break;
                case (R.id.camera2):
                    idCamera = 1;
                    break;
                case (R.id.camera3):
                    idCamera = 2;
                    break;
                case (R.id.camera4):
                    idCamera = 3;
                    break;
                default:
                    idCamera = 4;
                    break;
            }
            CameraActions cameraActions = new CameraActions(context);

            String url = cameraActions.selectCameras().get(idCamera).getIP();

            ConnectionTest task = new ConnectionTest();
            String[] params = new String[1];
            params[0] = url;
            try {
                if(task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,params).get()) {
                    new DownloadImagemAsyncTask().execute(
                            url + "image.jpg"
                    );
                }
                else{
                    System.out.println("erro");
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(ImageActivity.this).
                                    setTitle("Erro").
                                    setMessage("Não foi possivel carregar imagem. Verifique o IP e tente novamente mais tarde!").
                                    setPositiveButton("OK", null);
                    builder.create().show();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    /*    // Salva a URL para utiliza-la quando essa tela for re-aberta
        SharedPreferences preferencias = getSharedPreferences(
                "configuracao", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("url_imagem", edtUrl.getText().toString());
        editor.commit();*/
    }

    public void baixarImagemClick(View v){

        CameraActions cameraActions = new CameraActions(this);
        new DownloadImagemAsyncTask().execute(
                cameraActions.selectCameras().get(idCamera).getIP() + "image.jpg"
        );
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(thr1 != null && thr1.isAlive()){
            System.out.println("pausing");
            thr1.interrupt();
        }
        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onResume(){
        super.onResume();
        if(thr1 != null)
            try {
                thr1.notify();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    class DownloadImagemAsyncTask extends
            AsyncTask<String, Void, Bitmap>{

        //ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  dialog = ProgressDialog.show(
            //         ImageActivity.this,
            //         "Aguarde", "Carregando a  imagem...");
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String urlString = params[0];

            try {
                URL url = new URL(urlString);
                HttpURLConnection conexao = (HttpURLConnection)
                        url.openConnection();
                conexao.setRequestMethod("GET");
                conexao.setDoInput(true);

                conexao.connect();

                InputStream is = conexao.getInputStream();
                Bitmap imagem = BitmapFactory.decodeStream(is);
                return imagem;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            //dialog.dismiss();
            if (result != null){
                ImageView img = (ImageView)findViewById(R.id.imageView1);
                img.setImageBitmap(result);
                thr1 = Thread.currentThread();
                try {
                    Thread.sleep(1000);
                    baixarImagemClick(view);
                } catch (InterruptedException ex) {
                    System.out.println ("Thread pausada");
                }
            } else {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(ImageActivity.this).
                                setTitle("Erro").
                                setMessage("Não foi possivel carregar imagem, tente novamente mais tarde!").
                                setPositiveButton("OK", null);
                builder.create().show();
            }
        }
    }
}