package com.example.pentalab.cameraapp;

        import android.os.AsyncTask;

        import java.net.SocketTimeoutException;
        import java.net.URL;
        import java.net.URLConnection;

/**
 * Created by Femin on 02/09/2016.
 */
public class ConnectionTest  extends AsyncTask<String, Void, Boolean> {


    @Override
    protected Boolean doInBackground(String... params) {
        try{
            URL myUrl = new URL(params[0]);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(5000);
            connection.connect();

            return true;
        }catch (SocketTimeoutException e) {
            return false;
        }catch(Exception e) {
            return false;
        }
    }
}
