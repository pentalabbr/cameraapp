package com.example.pentalab.cameraapp;

import android.os.AsyncTask;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Guillermo on 30-Aug-16.
 */
public class ConnectionTask extends AsyncTask<String, Void, String> {

    String connectok = "";
    @Override
    protected String doInBackground(String... param) {


        try {
            //String url = "http://192.168.0.12/setSystemMotion";
            HttpURLConnection con = (HttpURLConnection)(new URL(param[0])).openConnection();

//            String userPassword = "admin:123456";
//            byte[] encodedBytes =  (userPassword.getBytes(),Base64.NO_PADDING);
//            System.out.println(encodedBytes);
//            String   userPassword = "Basic " + "YWRtaW46MTIzNDU2";
//            con.setRequestProperty("Authorization",   userPassword);
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            //con.setConnectTimeout(500);

            String urlParameters = param[1];
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());

            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();


            //toastactive.show();
//            int responseCode = con.getResponseCode();
//
//            System.out.println("\nSending 'POST' request to URL : " + param[0]);
//            System.out.println("Post parameters : " + urlParameters);
//            System.out.println("Response Code : " + responseCode);
//            System.out.println("Response mess : " +  con.getResponseMessage());

            connectok = con.getResponseMessage();
            if (!con.getResponseMessage().toLowerCase().contains("ok")) {
                return "false";
            }
        } catch (IOException e1) {
            //Toast t = Toast.makeText(,"Timeout error problem connection",Toast.LENGTH_SHORT);
            //t.show();
            e1.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
            connectok = "false";
        }

        return connectok.toLowerCase();
    }
}
