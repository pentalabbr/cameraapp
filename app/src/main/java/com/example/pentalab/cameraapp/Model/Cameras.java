package com.example.pentalab.cameraapp.Model;

/**
 * Created by Femin on 31/08/2016.
 */
public class Cameras {

    public Cameras(String ip, String nome){
        this.Nome = nome;
        this.IP = ip;
    }

    public Cameras(String ip, String nome, int id){
        this.Id = id;
        this.Nome = nome;
        this.IP = ip;
    }

    public Cameras(){}

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    @Override
    public String toString(){
        return getId()+"   Localização: " + getNome();
    }

    private int Id;
    private String IP;
    private String Nome;
}
