package com.example.pentalab.cameraapp.Model;

/**
 * Created by Femin on 29/08/2016.
 */
public class AppConfig {

    public AppConfig(String usuario, String senha){
        this.Usuario = usuario;
        this.Senha = senha;
    }

    public AppConfig(){}

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String senha) {
        Senha = senha;
    }

    public int getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    private int Id;
    private String Usuario;
    private String Senha;
}