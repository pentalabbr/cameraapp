package com.example.pentalab.cameraapp.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pentalab.cameraapp.DbActions.CameraActions;
import com.example.pentalab.cameraapp.Model.Cameras;
import com.example.pentalab.cameraapp.R;

import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private ListView listViewCameras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        listViewCameras = (ListView) findViewById(R.id.cameraListView);
    }

    public void editaCamera (View v){
        setContentView(R.layout.activity_cadastrar_camera);
        CameraActions cameraActions = new CameraActions(this);
        int id;
        switch (v.getId()) {
            case (R.id.btnEditarCamera):
                id = 0;
                break;
            case (R.id.btnEditarCamera2):
                id = 1;
                break;
            case (R.id.btnEditarCamera3):
                id = 2;
                break;
            case (R.id.btnEditarCamera4):
                id = 3;
                break;
            default:
                id = 4;
                break;
        }

        Cameras camera = cameraActions.selectCameras().get(id);
        setContentView(R.layout.activity_cadastrar_camera);
        EditText cameraNome = (EditText) findViewById(R.id.cameraNome);
        cameraNome.setText(camera.getNome());
        EditText cameraIp = (EditText) findViewById(R.id.cameraIp);
        cameraIp.setText(camera.getIP());
        TextView textView = (TextView) findViewById(R.id.cameraId);
        textView.setText(camera.getId() + "");
        textView.setVisibility(View.INVISIBLE);
;
    }

    public void editar(View v){

        EditText cameraNome = (EditText) findViewById(R.id.cameraNome);
        EditText cameraIp = (EditText) findViewById(R.id.cameraIp);
        TextView textView = (TextView) findViewById(R.id.cameraId);

        CameraActions cameraActions = new CameraActions(this);
        cameraActions.updateCameras(new Cameras(cameraIp.getText().toString(),cameraNome.getText().toString(),Integer.parseInt(textView.getText().toString())));

        setContentView(R.layout.activity_camera);
        listViewCameras = (ListView) findViewById(R.id.cameraListView);
        onResume();

    }

    @Override
    protected void onResume(){
        super.onResume();

        CameraActions cameraActions = new CameraActions(this);
        List<Cameras> camerasList = cameraActions.selectCameras();

        ArrayAdapter<Cameras> adp = new ArrayAdapter<Cameras>(this, android.R.layout.simple_list_item_1, camerasList);
        listViewCameras.setAdapter(adp);
    }
}
