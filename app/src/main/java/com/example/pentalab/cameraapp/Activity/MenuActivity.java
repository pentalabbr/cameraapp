package com.example.pentalab.cameraapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.pentalab.cameraapp.Activity.GerenciarCamerasActivity;
import com.example.pentalab.cameraapp.Activity.ImageActivity;
import com.example.pentalab.cameraapp.R;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void redirecionarImageActivity(View view){
        Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
        startActivity(intent);
    }

    public void redirecionarConfiguracaoActivity(View view){
        Intent intent = new Intent(getApplicationContext(), ConfiguracoesActivity.class);
        startActivity(intent);
    }

    public void redirecionarGerenciarCamerasActivity(View view){
        Intent intent = new Intent(getApplicationContext(), GerenciarCamerasActivity.class);
        startActivity(intent);
    }

    public void cadastrarCameras(View view){
        Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
        startActivity(intent);
    }

}
