package com.example.pentalab.cameraapp.DbActions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.pentalab.cameraapp.Model.AppConfig;
import com.example.pentalab.cameraapp.Model.Cameras;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Femin on 31/08/2016.
 */

public class CameraActions extends DbHelper{


    public CameraActions(Context context) {
        super(context);
    }

    public void insertCamera(Cameras camera){
        SQLiteDatabase db = getWritableDatabase();


        ContentValues cv = new ContentValues();
        cv.put("Nome", camera.getNome());
        cv.put("Ip", camera.getIP());
        db.insert("Cameras", null, cv);
        db.close();
    }

    public List<Cameras> selectCameras(){

        List<Cameras> lista = new ArrayList<Cameras>();
        SQLiteDatabase db = getReadableDatabase();
        String sqlSelectAll = "SELECT * FROM Cameras";
        Cursor c = db.rawQuery(sqlSelectAll, null);

        if(c.moveToFirst()){
            do {
                Cameras camera = new Cameras();
                camera.setId(c.getInt(0));
                camera.setNome(c.getString(1));
                camera.setIP(c.getString(2));

                lista.add(camera);
            }while (c.moveToNext());
        }
        db.close();
        return lista;
    }

    public void updateCameras(Cameras camera){

        SQLiteDatabase db = getWritableDatabase();
        String qryUpdate =  "update Cameras "
                            +"set Ip = '" + camera.getIP() +"',"
                            +"  Nome ='" + camera.getNome() +"'"
                            +" where Id =" + camera.getId();
        db.execSQL(qryUpdate);

    }

}
