package com.example.pentalab.cameraapp.DbActions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.pentalab.cameraapp.Model.AppConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Femin on 31/08/2016.
 */

public class AppConfigActions extends DbHelper{


    public AppConfigActions(Context context) {
        super(context);
    }

    public void insertAppConfig(AppConfig appConfig){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("Usuario", appConfig.getUsuario());
        cv.put("Senha", appConfig.getSenha());


        db.insert("AppConfig",null,cv);
        db.close();
    }

    public List<AppConfig> selectAppConfig(){

        List<AppConfig> lista = new ArrayList<AppConfig>();
        SQLiteDatabase db = getReadableDatabase();
        String sqlSelectAll = "SELECT * FROM AppConfig";
        Cursor c = db.rawQuery(sqlSelectAll, null);

        if(c.moveToFirst()){
            do {
                AppConfig appConfig = new AppConfig();
                appConfig.setId(c.getInt(0));
                appConfig.setUsuario(c.getString(1));
                appConfig.setSenha(c.getString(2));

                lista.add(appConfig);
            }while (c.moveToNext());
        }
        db.close();
        return lista;
    }

    public void updateAppConfig(AppConfig appConfig){
        SQLiteDatabase db = getWritableDatabase();
        String qryUpdate =  "update AppConfig "
                +"set Usuario = '" + appConfig.getUsuario() + "', "
                +"  Senha ='" + appConfig.getSenha() +"'";
        db.execSQL(qryUpdate);
        db.close();
    }


}
